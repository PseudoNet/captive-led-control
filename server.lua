srv=net.createServer(net.TCP)
srv:listen(80,function(conn) 

   local rnrn=0
   local Status = 0
   local DataToGet = 0
   local method=""
   local url=""
   local vars=""

  conn:on("receive",function(conn,payload)
  
    if Status==0 then
        _, _, method, url, vars = string.find(payload, "([A-Z]+) /([^?]*)%??(.*) HTTP")
        print(method, url, vars)                          
    end

	if url==nil then
		url="index.html"
	end
	
	if url=="" then
		url="index.html"
	end
	
	-- some ugly magic for Apple IOS Devices
	if string.find(url, "/") ~= nil then
	 --print ("Slash found")
	 local invurl=string.reverse(url)
	 local a,b=string.find(invurl, "/", 1)
	 url=string.sub(url, string.len(url)-(a-2))
	 --print ("Neue URL= " .. url)
	end
		
	if string.len(url)>= 25 then
		url = string.sub (url,1,25)
	--	print ("cut down URL")
	end
	
   
    DataToGet = -1

    conn:send("HTTP/1.1 200 OK\r\n\r\n")

	
	local foundmatch = 0
	local a = {'index.html','kitt.css','kitt.js','favicon.ico'}
	for _,v in pairs(a) do
		if v == url then
			foundmatch=1
			print ("Found " .. v)
			break
		end
	end

    if url=="setColour" then
        print("Setting Colour")
		print(payload)

        local rgbMap = {}
        if (payload ~= nil) then
            for key, value in string.gmatch(payload, "(%w+)=(%w+)&*") do
                rgbMap[key]=value
            end
            PATTERN_COLOUR_COMPONENT_RED=rgbMap["R"]
            PATTERN_COLOUR_COMPONENT_GREEN=rgbMap["G"]
            PATTERN_COLOUR_COMPONENT_BLUE=rgbMap["B"]
        end
    end

    if url=="startPattern" then
        print("Starting Pattern")
        startChase()
    end

    if url=="stopPattern" then
        print("Stopping Pattern")
        tmr.stop(PATTERN_TIMMER_ID)
    end

    if url=="increaseSpeed" then
        print("Increasing Speed "..PATTERN_SPEED)
        if PATTERN_SPEED>PATTERN_SPEED_DELAY_MIN then
            tmr.stop(PATTERN_TIMMER_ID)
            PATTERN_SPEED=PATTERN_SPEED-PATTERN_SPEED_DIFFERENCE
            startChase()
        end
    end

    if url=="decreaseSpeed" then
        print("Decreasing Speed: "..PATTERN_SPEED)
        if PATTERN_SPEED<PATTERN_SPEED_DELAY_MAX then
            tmr.stop(PATTERN_TIMMER_ID)
            PATTERN_SPEED=PATTERN_SPEED+PATTERN_SPEED_DIFFERENCE
            startChase()
        end
    end

    if foundmatch == 0 then
    	-- print ("Found no match, setting index")
        url="index.html"
    end
		
    -- it wants a file in particular
    if url~="" then
        DataToGet = 0
        return
    end    

  
  end)
  
  conn:on("sent",function(conn) 
    if DataToGet>=0 and method=="GET" then
        if file.open(url, "r") then            
            file.seek("set", DataToGet)
            local line=file.read(512)
            file.close()
            if line then
                conn:send(line)
				-- print ("sending:" .. DataToGet)
                DataToGet = DataToGet + 512    
                if (string.len(line)==512) then
                    return
                end
            end
        end        
    end

    conn:close() 
  end)
end)

	
print("listening, free:", node.heap())

