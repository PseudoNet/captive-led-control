
PIXELS     = 8
LED_PIN    = 4       -- GPIO2

PATTERN_COLOUR_COMPONENT_RED = 255
PATTERN_COLOUR_COMPONENT_GREEN = 0
PATTERN_COLOUR_COMPONENT_BLUE = 0
PATTERN_COLOUR_BLACK = string.char(0,0,0)

PATTERN_BRIGHTNESS_DIVIDER_HIGH = 1
PATTERN_BRIGHTNESS_DIVIDER_MED = 4
PATTERN_BRIGHTNESS_DIVIDER_LOW = 8

PATTERN_SPEED = 150
PATTERN_SPEED_DIFFERENCE = 25
PATTERN_SPEED_DELAY_MIN = 25
PATTERN_SPEED_DELAY_MAX = 300
PATTERN_TIMMER_ID = 0


function startChase()
    tmr.alarm(PATTERN_TIMMER_ID, PATTERN_SPEED, 1, function()
        coroutine.resume(chaseCO)
    end)
end

chaseCO = coroutine.create(function()
    print "Starting chase"
    activePixel = 0
    forward = true
    while true do
        buffer = ""
        for pixel = 0, (PIXELS-1) do
            if (pixel==activePixel) then
                buffer = buffer..string.char(
                                     PATTERN_COLOUR_COMPONENT_BLUE/PATTERN_BRIGHTNESS_DIVIDER_HIGH
                                    ,PATTERN_COLOUR_COMPONENT_RED/PATTERN_BRIGHTNESS_DIVIDER_HIGH
                                    ,PATTERN_COLOUR_COMPONENT_GREEN/PATTERN_BRIGHTNESS_DIVIDER_HIGH)
            else
                if (forward) then
                    if (pixel==(activePixel-1)) then
                        buffer = buffer..string.char(
                                             PATTERN_COLOUR_COMPONENT_BLUE/PATTERN_BRIGHTNESS_DIVIDER_MED
                                            ,PATTERN_COLOUR_COMPONENT_RED/PATTERN_BRIGHTNESS_DIVIDER_MED
                                            ,PATTERN_COLOUR_COMPONENT_GREEN/PATTERN_BRIGHTNESS_DIVIDER_MED)
                    elseif (pixel==(activePixel-2)) then
                       buffer = buffer..string.char(
                                             PATTERN_COLOUR_COMPONENT_GREEN/PATTERN_BRIGHTNESS_DIVIDER_LOW
                                            ,PATTERN_COLOUR_COMPONENT_RED/PATTERN_BRIGHTNESS_DIVIDER_LOW
                                            ,PATTERN_COLOUR_COMPONENT_GREEN/PATTERN_BRIGHTNESS_DIVIDER_LOW)
                    else
                        buffer = buffer..PATTERN_COLOUR_BLACK
                    end
                else
                    if (pixel==(activePixel+1)) then
                        buffer = buffer..string.char(
                                             PATTERN_COLOUR_COMPONENT_BLUE/PATTERN_BRIGHTNESS_DIVIDER_MED
                                            ,PATTERN_COLOUR_COMPONENT_RED/PATTERN_BRIGHTNESS_DIVIDER_MED
                                            ,PATTERN_COLOUR_COMPONENT_GREEN/PATTERN_BRIGHTNESS_DIVIDER_MED)
                    elseif (pixel==(activePixel+2)) then
                        buffer = buffer..string.char(
                                             PATTERN_COLOUR_COMPONENT_BLUE/PATTERN_BRIGHTNESS_DIVIDER_LOW
                                            ,PATTERN_COLOUR_COMPONENT_RED/PATTERN_BRIGHTNESS_DIVIDER_LOW
                                            ,PATTERN_COLOUR_COMPONENT_GREEN/PATTERN_BRIGHTNESS_DIVIDER_LOW)
                    else
                        buffer = buffer..PATTERN_COLOUR_BLACK
                    end
                end
            end
        end
        
        ws2812.write(LED_PIN, buffer)
        if activePixel==(PIXELS-1) and forward then
            forward=false
        elseif activePixel==0 and not forward then
            forward=true
        end

        if forward then
           activePixel = activePixel+1 
        else
           activePixel = activePixel-1
        end 

        --print("Active Pixel = "..activePixel)
        coroutine.yield()
   end
end)


