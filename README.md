# captive-led-control
Captive portal for controlling a WS2812 LED light strip via an ESP8266 (ESP-02) module.
All the "captive portal" componets directly use, or are heavily based on this project: https://github.com/reischle/CaptiveIntraweb

Instructions for use:
Flash the customised firmware using esptool (https://github.com/themadinventor/esptool):
```
sudo ./esptool.py --port /dev/ttyUSB0 write_flash 0x00000 ./0x00000.bin 0x10000 ./0x10000.bin
```

Use ESPlorer (http://esp8266.ru/esplorer/#download) to connect and upload each of the files:
 - ap-mode.lua
 - dns-liar.lua
 - favicon.ico
 - index.html
 - init.lua
 - kitt.css
 - kitt.js
 - led.lua
 - server.lua

Run dns-liar.lua manually once by clicing the "Reload" button on the right hand side of ESPlorer then clicking on the displayed filename(dns-liar.lua).

After clicking the reset button, you should be able to connect to the AP ("KITT") and control the connected WS2812 LED light strip.
