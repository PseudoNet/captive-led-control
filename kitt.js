

function setColour(R,G,B){
	httpGetAsync("/setColour?R="+R+"&G="+G+"&B="+B)
}
function startPattern(){
	httpGetAsync("/startPattern")
}
function stopPattern(){
	httpGetAsync("/stopPattern")
}
function increaseSpeed(){
	httpGetAsync("/increaseSpeed")
}
function decreaseSpeed(){
	httpGetAsync("/decreaseSpeed")
}


function httpGetAsync(url)
{

    var xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() { 
        if (xmlHttp.readyState == 4 && xmlHttp.status == 200){
            console.log ("Successful GET Request: "+url);
        }
    }
    xmlHttp.open("GET", url, true); // true for asynchronous 
    xmlHttp.send(null);
}

